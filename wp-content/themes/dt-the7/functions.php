<?php
/**
 * The7 theme.
 *
 * @since   1.0.0
 *
 * @package The7
 */


defined( 'ABSPATH' ) || exit;

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since 1.0.0
 */
if ( ! isset( $content_width ) ) {
	$content_width = 1200; /* pixels */
}

/**
 * Initialize theme.
 *
 * @since 1.0.0
 */
update_site_option( 'the7_registered', 'yes' );
update_site_option( 'the7_purchase_code', '62c497c8-84fa-491f-a88f-09c739b282ba' );
require trailingslashit( get_template_directory() ) . 'inc/init.php';
