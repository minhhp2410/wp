<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'bli_the7' );

/** MySQL database username */
define( 'DB_USER', 'admin' );

/** MySQL database password */
define( 'DB_PASSWORD', 'admin' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'kAlzIfV*i[<!/~K25=HPlQmx>MOr#F91KY+`w^N!2~!PJJGL!3#Xncnq1rt[1V+,' );
define( 'SECURE_AUTH_KEY',  'vB=$O)/TS|37D9UYQS[#Ici>7i%;rS`Q~kWz##JPq`Qq*&ydOQUx3Fh=YkeduKCX' );
define( 'LOGGED_IN_KEY',    'Sf_K%(ZrX|B`|zjq9{hYTWWA{i8;7[jd*i?rVuNPJ;<Qc^3^cDv@&6B,5n7kKVYm' );
define( 'NONCE_KEY',        'f@k{5j%PYTM6M3,8@G9MrKqpx#|GYe4Dq&{+EU.iB?bHr[qbsB.0YEuf-/0Q:o^<' );
define( 'AUTH_SALT',        'CWiPQHXpC3?5BI-gH|0I,769:9@{9:jIjrVPUe0rT~|jtg_,(,mtMr#QTcu-[$%&' );
define( 'SECURE_AUTH_SALT', '-@yhF64s$ql5?DFaqiHU-n),=n&i}G1Zv=X2KV71%*R*XP9#d]z6-9<N1`-7v|f+' );
define( 'LOGGED_IN_SALT',   'lw0s[bG?8k>MJ`3Yfdv{:mU.O+RM^cUheQw*YQv&.j<POeeg58,|mflBA.i~yzS^' );
define( 'NONCE_SALT',       '$3^]T%|!H;$5~o_rMdsw^6bo$$kQ{`()E:JWEV?rIKfG+EUEyx+b]UI?_C@eRH:V' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
